# Dockerized WordPlate
This is a base image for dockerized WordPlate sites.

Build a WordPlate site (`composer install` etc) and bake it into a descending image.
Then use that image in docker-compose.yml, adding one service with `["php-fpm"]` command, another with `["nginx", "-g", "daemon off;"]` command. Attach a volume with uploads. Now you have a transient container with code and state in uploads volume.

Pay attention that currently trusted proxy IP that passes real IPs in X-Real-IP field is hardcoded in nginx/nginx.conf
