# XDebug instructions start
# for enabling xdebug, uncomment this
# RUN yes | pecl install xdebug-2.6.0
# and add this xdebug.ini file to php-fpm folder:
# --- start xdebug.ini ---
# zend_extension=xdebug.so
# xdebug.remote_enable=0
# xdebug.remote_autostart=0
# xdebug.remote_host=docker.for.mac.localhost
# xdebug.remote_port=9005
# xdebug.show_local_vars=1
# xdebug.collect_params=4
# ; xdebug.remote_connect_back=1
#
# ; add XDEBUG_SESSION_START=debug_key as parameter to the URL
# xdebug.idekey=debug_key
#
# ; profiling
# xdebug.profiler_enable=1
# xdebug.profiler_output_dir=/cachegrind
# ; xdebug.profiler_append=1
#
# ; Fails with this options: child 40 exited on signal 11 (SIGSEGV)
# ; xdebug.profiler_aggregate=1
#
# ; put everything in one file. move away old one every time!
# ; xdebug.profiler_aggregate=1
# --- end xdebug.ini ---

# for profiling, replace xdebug.ini above with:
# --- start xdebug.ini ---
# zend_extension=xdebug.so
# xdebug.profiler_enable=1
# xdebug.profiler_output_dir=/cachegrind
#
# ; xdebug.profiler_append=1
#
# ; Output into single file. Unfortunately, it
# ; fails with this option: child 40 exited on signal 11 (SIGSEGV)
# ; xdebug.profiler_aggregate=1
# --- end xdebug.ini ---

# XDebug instructions end
