FROM php:7.2-fpm

# Fix obvious
ENV LANG C.UTF-8

# php-fpm start
# install the PHP extensions we need
RUN set -ex; \
	\
	apt-get update; \
	apt-get install -y \
		libjpeg-dev \
		libpng-dev \
    curl \
    git \
    openssh-client \
    openssl \
    zlib1g-dev \
    vim \
    mysql-client \
	; \
	rm -rf /var/lib/apt/lists/*; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install gd mysqli opcache zip
# TODO consider removing the *-dev deps and only keeping the necessary lib* packages

COPY php-fpm/ /usr/local/etc/php/conf.d/
# php-fpm end

# Installing WP-CLI
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
  && chmod +x wp-cli.phar && mv wp-cli.phar /usr/local/bin/wp

# nginx start
# Start of mainline/stretch/Dockerfile from nginxinc/docker-nginx repo
# (it's based on the same debian:stretch-slim as php:7.2-fpm is)

ENV NGINX_VERSION 1.13.8-1~stretch
ENV NJS_VERSION   1.13.8.0.1.15-1~stretch

RUN set -x \
	&& apt-get update \
	&& apt-get install --no-install-recommends --no-install-suggests -y gnupg1 \
	&& \
	NGINX_GPGKEY=573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62; \
	found=''; \
	for server in \
		ha.pool.sks-keyservers.net \
		hkp://keyserver.ubuntu.com:80 \
		hkp://p80.pool.sks-keyservers.net:80 \
		pgp.mit.edu \
	; do \
		echo "Fetching GPG key $NGINX_GPGKEY from $server"; \
		apt-key adv --keyserver "$server" --keyserver-options timeout=10 --recv-keys "$NGINX_GPGKEY" && found=yes && break; \
	done; \
	test -z "$found" && echo >&2 "error: failed to fetch GPG key $NGINX_GPGKEY" && exit 1; \
	apt-get remove --purge --auto-remove -y gnupg1 && rm -rf /var/lib/apt/lists/* \
	&& dpkgArch="$(dpkg --print-architecture)" \
	&& nginxPackages=" \
		nginx=${NGINX_VERSION} \
		nginx-module-xslt=${NGINX_VERSION} \
		nginx-module-geoip=${NGINX_VERSION} \
		nginx-module-image-filter=${NGINX_VERSION} \
		nginx-module-njs=${NJS_VERSION} \
	" \
	&& case "$dpkgArch" in \
		amd64|i386) \
# arches officialy built by upstream
			echo "deb http://nginx.org/packages/mainline/debian/ stretch nginx" >> /etc/apt/sources.list \
			&& apt-get update \
			;; \
		*) \
# we're on an architecture upstream doesn't officially build for
# let's build binaries from the published source packages
			echo "deb-src http://nginx.org/packages/mainline/debian/ stretch nginx" >> /etc/apt/sources.list \
			\
# new directory for storing sources and .deb files
			&& tempDir="$(mktemp -d)" \
			&& chmod 777 "$tempDir" \
# (777 to ensure APT's "_apt" user can access it too)
			\
# save list of currently-installed packages so build dependencies can be cleanly removed later
			&& savedAptMark="$(apt-mark showmanual)" \
			\
# build .deb files from upstream's source packages (which are verified by apt-get)
			&& apt-get update \
			&& apt-get build-dep -y $nginxPackages \
			&& ( \
				cd "$tempDir" \
				&& DEB_BUILD_OPTIONS="nocheck parallel=$(nproc)" \
					apt-get source --compile $nginxPackages \
			) \
# we don't remove APT lists here because they get re-downloaded and removed later
			\
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
# (which is done after we install the built packages so we don't have to redownload any overlapping dependencies)
			&& apt-mark showmanual | xargs apt-mark auto > /dev/null \
			&& { [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; } \
			\
# create a temporary local APT repo to install from (so that dependency resolution can be handled by APT, as it should be)
			&& ls -lAFh "$tempDir" \
			&& ( cd "$tempDir" && dpkg-scanpackages . > Packages ) \
			&& grep '^Package: ' "$tempDir/Packages" \
			&& echo "deb [ trusted=yes ] file://$tempDir ./" > /etc/apt/sources.list.d/temp.list \
# work around the following APT issue by using "Acquire::GzipIndexes=false" (overriding "/etc/apt/apt.conf.d/docker-gzip-indexes")
#   Could not open file /var/lib/apt/lists/partial/_tmp_tmp.ODWljpQfkE_._Packages - open (13: Permission denied)
#   ...
#   E: Failed to fetch store:/var/lib/apt/lists/partial/_tmp_tmp.ODWljpQfkE_._Packages  Could not open file /var/lib/apt/lists/partial/_tmp_tmp.ODWljpQfkE_._Packages - open (13: Permission denied)
			&& apt-get -o Acquire::GzipIndexes=false update \
			;; \
	esac \
	\
	&& apt-get install --no-install-recommends --no-install-suggests -y \
						$nginxPackages \
						gettext-base \
	&& rm -rf /var/lib/apt/lists/* \
	\
# if we have leftovers from building, let's purge them (including extra, unnecessary build deps)
	&& if [ -n "$tempDir" ]; then \
		apt-get purge -y --auto-remove \
		&& rm -rf "$tempDir" /etc/apt/sources.list.d/temp.list; \
	fi

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80

STOPSIGNAL SIGTERM

# End mainline/stretch/Dockerfile from nginxinc/docker-nginx repo

# Copy nginx configs.
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
# nginx end

# --- START COMPOSER INSTALLATION
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp
ENV COMPOSER_VERSION 1.6.3

RUN curl -s -f -L -o /tmp/installer.php https://raw.githubusercontent.com/composer/getcomposer.org/b107d959a5924af895807021fcef4ffec5a76aa9/web/installer \
 && php -r " \
    \$signature = '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061'; \
    \$hash = hash('SHA384', file_get_contents('/tmp/installer.php')); \
    if (!hash_equals(\$signature, \$hash)) { \
        unlink('/tmp/installer.php'); \
        echo 'Integrity check failed, installer is either corrupt or worse.' . PHP_EOL; \
        exit(1); \
    }" \
 && php /tmp/installer.php --no-ansi --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION} \
 && composer --ansi --version --no-interaction && rm -rf /tmp/* /tmp/.htaccess
# --- STOP COMPOSER INSTALLATION


ARG WORDPLATE_VERSION=6.2.0

# `git clone` followed by `composer install` is equivalent to
# `composer create-project`, but this way I don't have to install twice
# (first the initial composer.json dependencies from wordplate repo, then mine).
RUN git clone --branch $WORDPLATE_VERSION --depth 1 \
    https://github.com/wordplate/wordplate /wordplate && rm -rf /var/www \
    && mv /wordplate /var/www
# => /var/www/public will be an nginx root

WORKDIR /var/www

ONBUILD COPY deployment-keys /root/.ssh
ONBUILD RUN chmod 0700 /root/.ssh/ && chmod 0600 /root/.ssh/*

# Overwrite composer.json and install actual dependencies in a downstream build
ONBUILD COPY composer.json ./
ONBUILD RUN composer update

# Otherwise uploads volume is mounted with root owner
# see https://github.com/moby/moby/issues/2259#issuecomment-48286811
ONBUILD RUN mkdir -p public/uploads && chgrp -R www-data /var/www

    # r-xr-x--- for all directories,
ONBUILD RUN find /var/www -type d -exec chmod 0550 {} \; \
    # r--r--- for all files,
    && find /var/www -type f -exec chmod 0440 {} \; \
    # but...
    # rwxrwx--- for uploads directories,
    && find /var/www/public/uploads -type d -exec chmod 0770 {} \; \
    # rw-rw---- for upload files
    && find /var/www/public/uploads -type f -exec chmod 0660 {} \;

# Now this image can be brought to life with two containers: one running
# php-fpm, and the other one running nginx.
# Pay attention that php-fpm container url is hardcoded in nginx config
# (default.conf) as 'app:9000'. And it must resolve in nginx container by the
# time nginx starts, otherwise it will fail and exit.

# CMD ["php-fpm"]
# CMD ["nginx", "-g", "daemon off;"]

# Important!!! Make code read-only, otherwise in can deviate in nginx and fpm
# containers! If you can't, user seperviserd and run both in the same container.
