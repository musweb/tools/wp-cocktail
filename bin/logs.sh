#!/bin/sh

. bin/envs.sh

# assumes docker-compose is present on the runner machine
docker-compose -f tests/docker-compose.yml logs -f
