#!/bin/bash

. bin/envs.sh

COUNTER=0
STATUS=1
ATTEMPTS=15

until [ $STATUS -eq 0 ] || [ $COUNTER -eq $ATTEMPTS ]; do
  sleep 3
  (( COUNTER++ ))
  echo "Attempt #$COUNTER out of $ATTEMPTS"
  docker-compose -f tests/docker-compose.yml -f tests/docker-compose.cli.yml \
    run --rm cli
  STATUS=$?
done
