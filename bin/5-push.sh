#!/bin/sh

. bin/envs.sh

# This will fail in non-CI environment
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
# docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker tag $TMP_IMAGE $PUSH_IMAGE
docker push $PUSH_IMAGE

# Cleanup the temporary image
docker rmi $TMP_IMAGE
