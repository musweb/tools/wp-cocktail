#!/bin/sh

. bin/envs.sh

# assumes docker-compose is present on the runner machine
docker-compose -f tests/docker-compose.yml down

# Wipe everything
docker container prune -f && docker volume prune -f && docker network prune -f
