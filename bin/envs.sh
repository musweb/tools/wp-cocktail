#!/bin/sh

CI_COMMIT_SHA=${CI_COMMIT_SHA:-`git rev-parse --verify HEAD`}
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-'registry.gitlab.com/musweb/tools/wp-cocktail'}

export TMP_IMAGE="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
PUSH_IMAGE="$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:latest"

# CI_REGISTRY=${CI_REGISTRY:-'registry.gitlab.com'}
# CI_REGISTRY_USER=${CI_REGISTRY_USER:-'mvasin'}
