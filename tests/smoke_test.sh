#!/bin/bash

test_string="Just another WordPress site"

curl localhost | grep "$test_string"

rc=$?;

if [[ $rc != 0 ]]; then
  # show what could be wrong
  . bin/envs.sh
  docker-compose -f tests/docker-compose.yml logs --tail=100
  exit $rc
else
  echo The site homepage contains $test_string
fi
